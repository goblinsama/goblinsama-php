<?php

/*	Goblinsama-PHP Library
	https://bitbucket.org/goblinsama/goblinsama-php
	
	© 2016-2018 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

$user_level = array(
	 0 => 'any',
	 2 => 'banned',
	 5 => 'bot',
	 8 => 'unconfirmed', // Needs to be at least this level to login
	10 => 'user', // Needs to be at least this level to do any action save for logging in and confirming
	20 => 'mod',
	40 => 'admin',
	50 => 'owner',
);
$user_level_r = array_flip($user_level);
