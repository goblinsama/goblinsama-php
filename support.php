<?php

/*	Goblinsama-PHP Library
	https://bitbucket.org/goblinsama/goblinsama-php
	
	© 2016-2018 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

// get, arget
{
	function arget_x ($target) {
		$orig = $target;
		if (is_string($target)) $target=(int)$target;
		if (is_object($target)) $target=(array)$target;
		
		if (is_array($target) || is_int($target)) return $target;
		
		throw new UnexpectedValueException("Unknown type '$orig'[".gettype($orig)."] --> '$target'[".gettype($target)."]");
	}
}

// session
{
	function check_login ($user_id, $token) {
		($user = get_user_from_id($user_id)) or except('NotFoundException',"User [$user_id] not found.");
		($user['login_token'] === $token) or except('UnauthorizedException',"Login error with user [$user_id].");
		return $user;
	}
	function login ($user_id=NULL, $token=NULL) {
		global $logged_user;
		
		try {
			if ($user_id===NULL && isset($_SESSION['user_id'])) {
				$user_id = (int)$_SESSION['user_id'];
			}
			if ($token===NULL && isset($_SESSION['login_token'])) {
				$token = $_SESSION['login_token'];
			}
			
			if (!$user_id || !$token) {
				logout();
				return;
			}
			
			$logged_user = check_login($user_id,$token);
		}
		catch (Exception $ex) {
			logout();
			throw $ex;
		}
		
		$_SESSION['user_id'] = $user_id;
		$_SESSION['login_token'] = $token;
		
		return $logged_user;
	}
	function logout () {
		global $logged_user,$db,$DP;
		
		if (isset($_SESSION['user_id']) && $_SESSION['login_token']) {
			$user_id = (int)$_SESSION['user_id'];
			$token = $_SESSION['login_token'];
			
			try {
				if (check_login($user_id,$token)) {
					// pulisce il database
					$db->queryOne("UPDATE `{$DP}user` SET `login_token`=NULL, `last_update`=NOW() WHERE `user_id`=?
						", array( $user_id )
					);
				}
			}
			catch (Exception $ex) {}
		}
		
		unset($_SESSION['user_id']);
		unset($_SESSION['login_token']);
		
		$logged_user = NULL;
	}
}

// page
{
	function head ($title, $extra='') {
		global $APIURL,$OFFLINE;
		
		$ret='';
		
		// head
		{
			$ret.='
			<head>
			';
			
			if ($OFFLINE) {
				$ret.='
				<script src="/lib/offline/jquery-2.2.4.min.js"></script>
				<link rel="stylesheet" href="/lib/offline/bootstrap.min.css">
				<link rel="stylesheet" href="/lib/offline/bootstrap-theme.min.css">
				<script src="/lib/offline/bootstrap.min.js"></script>
				';
			}
			else {
				// jQuery
				$ret.='
				<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
				';
				
				// Bootstrap
				$ret.='
				<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
				<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
				<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
				';
			}
			
			$ret.='
				<link href="/lib/goblinsama-php/base.css" rel="stylesheet">
				<link href="/css/style.css" rel="stylesheet">
				<script src="/js/script.js"></script>
				<script src="/lib/goblinsama-php/page/page.js"></script>
				<script src="/lib/goblinsama-php/page/ext.js"></script>
				
				<title>'.ucfirst($title).'</title>
				
				'.$extra.'
				
			</head>
			';
		}
		
		$ret.='
		<script>
			var APIURL=\''.$APIURL.'\';
			var loading=\'<img class="loading" alt="loading..." src="/gfx/ajax_wait.gif">\';
		</script>
		';
		
		return $ret;
	}
	function page ($pathi, $page_title, $index=NULL, $extra=NULL) {
		global $PROGNAME;
		
		$page = new page('html');
		$page->sync();
		
		try {
			/// get
			
			if ($index===NULL) {
				$file_title= $page_title;
				$head = $page_title;
			}
			else {
				$file_title = "index$index";
				$head = NULL;
			}
			$filename = "pages/$file_title.php";
			
			/// head
			
			$page->add(head($head,$extra));
			$page->sync();
			
			// body head
			
			$page->add('
			<body id="'.$file_title.'">
				'.menu($head).'
				<hr>
				<div id="page_whole">
			');
			$page->sync();
			
			// include
			
			file_exists($filename) or except('NotFoundException',"File [$file_title] not found");
			require_once "$filename";
			$view_page = "view_$file_title";
			
			/// actual body
			
			$page->add('
					'.$view_page(pathi_to_array($pathi)).'
			');
		}
		catch (UnauthorizedException $ex) {
			$page->flush();
			$page->add('
				<div class="alert alert-danger">
					<div class="p">
						Unauthorized.
					</div>
					<div class="p">
						'.$ex.'
					</div>
				</div>
			');
		}
		catch (Exception $ex) {
			$page->flush();
			$page->add('<div class="alert alert-danger">'.exprint($ex).'</div>');
		}
		finally {
			$page->add('
				</div>
			');
			am_admin() and $page->add("
				<script>$('div.menu').css('visibility','visible');</script>
			");
			$page->add('
				<div class="footer">
					'.$PROGNAME.' © 2016-2017 <a href="https://goblinsama.com">Goblinsama Ltd.</a>, All rights reserved
				</div>
			</body>
			');
		}
		
		$page->end();
		
		return $page;
	}
	function title ($LINK, $page=NULL, $title=NULL) {
		global $PROGNAME,$pages;
		
		if ($page===NULL) {
			return $LINK ? '<a href="/">'.ucfirst($PROGNAME).'</a>' : ucfirst($PROGNAME);
		}
		else {
			if ($title===NULL) $title=ucfirst($page);
			else if ($page!==NULL && $title===TRUE) $title=$pages[$page];
			
			return $LINK ? '<a href="/view/'.$page.'">'.$title.'</a>' : $title;
		}
	}
	function menu ($page, $bkg=NULL) {
		global $pages_show,$pages_tra,$pages;
		
		$ret = '';
		
		if ($page!=NULL && !isset($pages[$page])) {
			$page = $pages_tra[$page];
		}
		
		$ret.='<h1>';
		$bkg and $ret.='<img src="'.$bkg.'">';
		$ret.=title( TRUE, NULL );
		if ($page) {
			$ret.=' – ';
			$ret.=title( TRUE, $page, TRUE );
		}
		$ret.='</h1>';
		
		$ret.='<div class="menu">';
		foreach ($pages_show as $id => $title) {
			if ($id==$page) {
				$LINK = FALSE;
				$CL = 'selected';
			}
			else {
				$LINK = TRUE;
				$CL = '';
			}
			$ret.='<span class="menu_item '.$CL.'">'.title( $LINK, $id, $title ).'</span>';
		}
		$ret.='</div>';
		
		return $ret;
	}
	function page_title_name ($page) {
		global $pages,$pages_tra;
		
		if (isset($pages[$page]))
			return $pages[$page];
		
		if (isset($pages_tra[$page]))
			return $pages[$pages_tra[$page]];
		
		throw new UnexpectedValueException("Unknown page [$page].");
	}
	function just_inserted_div ($text, $id) {
		return alert_div("$text <$id>.","alert-success");
	}
	function alert_div ($text, $class='') {
		return '
		<div class="alert '.$class.' sp1">
			<span class="close" data-dismiss="alert">&times;</span>
			'.$text.'
		</div>
		';
	}
	function message_div ($args) {
		$ret = '';
		isset($args['message']) and $ret.=alert_div( $args['message'], empty($args['message_class'])?'':$args['message_class'] );
		return $ret;
	}
	
	function textarea ($class, $id, $id_text, $placeholder, $value, $onclick, $rows=2, $onclick_text='Submit', $onclick2=NULL, $onclick2_text='Something') {
		return textarea_a([
			'class' => $class,
			'id' => $id,
			'id_text' => $id_text,
			'placeholder' => $placeholder,
			'value' => $value,
			'rows' => $rows,
			
			'onclick' => $onclick,
			'onclick_text' => $onclick_text,
			
			'onclick2' => $onclick2,
			'onclick2_text' => $onclick2_text,
		]);
	}
	function textarea_a ($args) {
		{
			$class='';
			$id='';
			$id_text='';
			$placeholder='';
			$value='';
			$rows=2;
			
			$onclick='';
			$onclick_text='Submit';
			$button_class='primary';
			
			$onclick2=NULL;
			$onclick2_text='Something';
		}
		
		{
			isset($args['class'])         and $class=$args['class'];
			isset($args['id'])            and $id=$args['id'];
			isset($args['id_text'])       and $id_text=$args['id_text'];
			isset($args['placeholder'])   and $placeholder=$args['placeholder'];
			isset($args['value'])         and $value=$args['value'];
			isset($args['rows'])          and $rows=$args['rows'];
			
			isset($args['onclick'])       and $onclick=$args['onclick'];
			isset($args['onclick_text'])  and $onclick_text=$args['onclick_text'];
			isset($args['button_class'])  and $button_class=$args['button_class'];
			
			isset($args['onclick2'])      and $onclick2=$args['onclick2'];
			isset($args['onclick2_text']) and $onclick2_text=$args['onclick2_text'];
		}
		
		$ret = '';
		$rows = max((int)$rows,1);
		
		$ret .= '
		<div class="input-group '.$class.'">
			<span class="input-group-addon" id="'.$id.'">'.$id_text.'</span>
			<textarea class="form-control" placeholder="'.$placeholder.'" id="'.$id.'-area" aria-describedby="'.$id.'" onkeyup="find_and_search(this);" rows='.$rows.'>'.$value.'</textarea>
		';
		$onclick2 and $ret.='
			<span class="input-group-btn">
				<span class="btn btn-secondary" onclick="'.$onclick2.'">'.$onclick2_text.'</span>
			</span>
		';
		$ret.='
			<span class="input-group-btn">
				<span class="btn btn-'.$button_class.'" onclick="'.$onclick.'">'.$onclick_text.'</span>
			</span>
		</div>
		';
		
		return $ret;
	}
}

// users
{
	function get_user_from_id ($id) {
		global $db,$DP;
		return $db->call('fetch',"SELECT * FROM `{$DP}user`
			WHERE `user_id`=?
			", array((int)$id)
		);
	}
	function get_user_from_tg_id ($tg_id) {
		global $db,$DP;
		return $db->call('fetch',"SELECT * FROM `{$DP}user`
			WHERE `tg_user_id`=?
			", array((int)$tg_id)
		);
	}
	function get_admins () {
		global $db,$DP,$user_level_r;
		return $db->call('fetchAll',"SELECT * FROM `{$DP}user`
			WHERE `user_level`>=?
			", array( $user_level_r['admin'] )
		);
	}
}

// game
{
	function _calc_value ($level, $stat_n) {
		global $unit_base;
		
		$mantissa = calc_mantissa($level);
		if ($mantissa==0) {
			$diff = $level-4;
			$base = $stat_n===NULL ? 1 : $unit_base[$stat_n];
			return pow(2,$diff)*$base;
		}
		else {
			// calcola la superiore, la inferiore, e fa una media pesata
			$low_p = 1-$mantissa;
			$high_p = $mantissa;
			
			$low = _calc_value(floor($level),$stat_n);
			$high = _calc_value(ceil($level),$stat_n);
			
			return $low*$low_p + $high*$high_p;
		}
	}
}

// misc
{
	function get_webhook_filename () {
		global $ROOT,$WH_TOK_RELF;
		return $ROOT.$WH_TOK_RELF;
	}
	function get_webhook_url ($token) {
		global $APIURL;
		return 'https:'.$APIURL.'webhook?update&token='.htmlspecialchars($token).'';
	}
}
