<?php

/*	Goblinsama-PHP Library
	https://bitbucket.org/goblinsama/goblinsama-php
	
	© 2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	© 2015-2018 Lorenzo Petrone "Lohoris" <looris+src@gmail.com> https://lohoris.net
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

class DuplicateException extends Exception {
	public function __construct ( $message = NULL ) {
		$this->message = $message ?? 'Duplicate';
		parent::__construct( $this->message );
	}
}
class NotFoundException extends RuntimeException {
	public function __construct ( $message = NULL ) {
		$this->message = $message ?? 'Not found';
		parent::__construct( $this->message );
	}
}
class UnauthorizedException extends RuntimeException {
	public function __construct ( $message = NULL ) {
		$this->message = $message ?? 'Unauthorized';
		parent::__construct( $this->message );
	}
}
class InvalidDateException extends Exception {
	public $date;
	public function __construct ($date=NULL) {
		if ($date!==NULL) {
			$this->date=$date;
		}
		parent::__construct("Invalid date <$date>.");
	}
}
class DatabaseException extends Exception {
	public function __construct ($arg1=NULL, $arg2=NULL) {
		$message = "";
		$obj = NULL;
		
		if ($arg1!==NULL) {
			if (is_string($arg1)) {
				$message = $arg1;
			}
			else if (is_object($arg1)) {
				$obj = $arg1;
			}
		}
		if ($arg2!==NULL) {
			if (is_string($arg2)) {
				$message = $arg2;
			}
			else if (is_object($arg2)) {
				$obj = $arg2;
			}
		}
		
		$msg = "Database error: $message";
		if (is_object($obj) && is_callable(array($obj,'getError'))) {
			$msg .= ("; ".$obj->getError());
		}
		parent::__construct($msg);
	}
}
class UnimplementedException extends LogicException {
	public function __construct ( $message = NULL ) {
		$this->message = $message ?? 'Not implemented';
		parent::__construct( $this->message );
	}
}
class SystemException extends Exception {
	public function __construct ( $message = NULL ) {
		$this->message = $message ?? 'System Exception';
		parent::__construct( $this->message );
	}
}
class InvalidArgumentsException extends Exception {
	public function __construct ( $message = NULL ) {
		$this->message = $message ?? 'Invalid arguments';
		parent::__construct( $this->message );
	}
}
class ApplicationException extends LogicException {
	public function __construct ( $message = NULL ) {
		$this->message = $message ?? 'Application Exception';
		parent::__construct( $this->message );
	}
}

function except ($arg1=NULL, $arg2=NULL, $arg3=NULL) {
	global $RG_DEBUG;
	
	$exc = 'Exception';
	$msg = "";
	
	if (class_exists($arg1)) {
		// TODO. dovrebbe anche controllare che derivi da Exception
		$exc=$arg1;
		$msg=$arg2;
		$adx=$arg3;
	}
	else if (is_string($arg1)) {
		$msg=$arg1;
		$adx=$arg2;
	}
	
	$admin_msg = ($RG_DEBUG && $adx!==NULL) ? $adx : '';
	
	throw new $exc("$admin_msg$msg");
}
