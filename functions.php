<?php

/*	Goblinsama-PHP Library
	https://bitbucket.org/goblinsama/goblinsama-php
	
	© 2017-2019 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	© 2006-2016 Lorenzo Petrone "Lohoris" <looris+src@gmail.com> https://lohoris.net
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

function _print_rx () {
	$ret='<pre>';
	$ret.='<p>'.print_r(func_get_args(),TRUE).'</p>';
	$ret.='</pre>';
	return $ret;
}
function print_rh () {
	return call_user_func_array( '_print_rx', recursive_escape( func_get_args() ) );
}
function print_rr () {
	return call_user_func_array('_print_rx',func_get_args());
}
function print_re () {
	echo call_user_func_array('_print_rx',func_get_args());
}
function print_rrd () {
	call_user_func_array('print_re',func_get_args());
	die();
}
function print_ed () {
	throw new Exception( call_user_func_array('print_rr',func_get_args()) );
}

function print_tr () {
	return print_r(func_get_args(),TRUE);
}
function print_te () {
	print_r(func_get_args());
}
function print_ted () {
	call_user_func_array('print_te',func_get_args());
	die();
}

function show_debug () {
	global $DEBUG_EX;
	return $DEBUG_EX || am_admin();
}
function exprint ( $ex ) {
	if ( show_debug() ) {
		$MAX_LEN = 8192;
		
		$ret = '';
		
		$ret .= '<p>'.$ex->getMessage().'</p>';
		
		$prr = print_rr( $ex );
		if (strlen($prr)>$MAX_LEN) {
			$prr = substr($prr,0,$MAX_LEN).'<br><br>[...]';
			tmplog("==== Long exception ====\n".print_rr($ex)."\n==== ==== ==== ====\n");
		}
		
		$ret .= '<p>'.$prr.'</p>';
		
		return $ret;
	}
	
	return $ex->getMessage();
}

function recursive_escape ( $target ) {
	if ( is_array( $target ) ) {
		$ret = array();
		foreach ( $target as $id => $child ) {
			$ret[$id] = recursive_escape( $child );
		}
		return $ret;
	}
	else {
		return htmlspecialchars( $target );
	}
}

// Return $length random bytes. NOT cryptographically safe.
function urandom ( $length, $text = false ) {
	$ret = random_bytes( $length );
	return $text ? bin2hex( $ret ) : $ret;
}

// Returns $string up to character $char.
function upto ( $string, $char='#' ) {
	$pos = strpos( $string, $char );
	if ( $pos === FALSE )
		return $string;
	return substr( $string, 0, $pos );
}

// Returns TRUE if $string starts with $start.
function strstart ( $string, $start ) {
	return ( substr( $string, 0, strlen( $start ) ) === $start );
}

// Removes everything save a-z characters.
function filter_az ( $string, $UPPER = FALSE, $EXTRA = "" ) {
	$EX = $UPPER ? 'A-Z' : '';
	return preg_replace( "/[^a-z{$EX}{$EXTRA}]+/", '', $string );
}

// Removes everything save 0-9 characters.
function filter_num ( $string ) {
	return preg_replace( "/[^0-9]+/", '', $string );
}

// Should strip slightly less than filter_az
function filter_latin ( $string ) {
	return preg_replace( '/[^\p{Latin}\d \'\-\&]/u', '', $string );
}

// Strips all whiltespace
function strip_whitespace ( $string ) {
	return preg_replace( '/\s/', '', $string );
}

// Converts all newlines to the only true format \n
function convert_newlines ( $string ) {
	return preg_replace( '~\R~u', "\n", $string );
}

// Like array_rand() but it returns the value instead of the key
function array_random ( $array ) {
	return $array[array_rand($array)];
}

// Shuffles an array without erasing the keys
function array_shuffle_assoc ( &$array ) {
	reset( $array );
	$keys = array_keys( $array );
	if ( !shuffle($keys) )
		return FALSE;
	
	$ret = array();
	foreach ( $keys as $key ) {
		$ret[$key] = $array[$key];
	}
	
	$array = $ret;
	reset( $array );
}

function array_merge_on ( $element ) {
	$arrays=func_get_args();
	array_shift($arrays); // elimina il primo elemento, che equivale a $element
	
	$ret=array();
	foreach ($arrays as $arr) {
		foreach ($arr as $elo) {
			$ela=(array)$elo;
			$ret[$ela[$element]]=$ela;
		}
	}
	return $ret;
}
function array_to_nv ( $source, $nam, $val ) {
	$ret = array();
	foreach ($source as $row) {
		if (is_object($row))
			$row = (array)$row;
		
		$ret[$row[$nam]] = $row[$val];
	}
	return $ret;
}

function array_average ( $array ) {
	return array_sum( $array ) / count( $array );
}

function random_alphanumeric ( $length, $letters=TRUE, $numbers=TRUE ) {
	$_letters = 'abcdefghijklmnopqrstuvwxyz';
	$_numbers = '0123456789';
	$characters = ($letters?$_letters:'').($numbers?$_numbers:'');
	
	$ret = '';
	for ($ii=0; $ii<$length; $ii++) {
		$ret.=$characters[mt_rand(0, strlen($characters)-1)];
	}
	return $ret;
}
function random_code ( $length, $block_size, $separator ) {
	$base = strtoupper(random_alphanumeric($length));
	
	$ret = NULL;
	for ($ii=0; $ii<$length; $ii+=$block_size) {
		$ret!==NULL and $ret.=$separator;
		$block = ($ii+$block_size <= $length) ? $block_size : ($length-$ii);
		$ret.=substr($base,$ii,$block);
	}
	return $ret;
}

function _control_group_text ($name, $text, $type, $extra) {
	return control_group($name, '
		<input type="'.$type.'" id="'.$name.'" placeholder="'.$text.'" '.$extra.'>
	');
}
function control_group_text ($name, $text, $extra='') {
	return _control_group_text($name,$text,'text',$extra);
}
function control_group_pass ($name, $text, $extra='') {
	return _control_group_text($name,$text,'password',$extra);
}
function control_group ($name, $core) {
	// NOTA: $core deve avere id pari a $name
	return '
	<div id="'.$name.'_div" class="control-group inline vertical-top">
		<div class="controls">
			'.$core.'
			<br>
			<span id="'.$name.'_help" class="help-inline" style="max-width: 100%;"></span>
		</div>
	</div>
	';
}
function select_from_array ($arr, $id, $zero=NULL, $selected=NULL, $extra="") {
	$ret='
	<select id="'.$id.'" name="'.$id.'" '.$extra.'>
	';
	if ($zero!==NULL) {
		$ret.='<option value=0'.($selected===0?' SELECTED':'').'>'.$zero.'</option>';
	}
	foreach ($arr as $aid => $nam) {
		$SEL = ($selected!==NULL && $selected==$aid) ? ' SELECTED' : '';
		$ret.='<option value='.$aid.$SEL.'>'.$nam.'</option>';
	}
	$ret.='
	</select>
	';
	return $ret;
}
function js_bool ($inc) {
	if ($inc=='true') {
		return true;
	}
	if ($inc=='false') {
		return false;
	}
	throw new UnexpectedValueException("Javascript error: not a proper boolean [$inc].");
}

function number_plus ($number, $equalzero=NULL) {
	if ($number<0) {
		return "$number";
	}
	if ($number==0) {
		return $equalzero===NULL ? "$number" : "$equalzero";
	}
	return "+$number";
}
function float_input ($input) {
	$num = str_replace(',','.',$input);
	$last_dot_post = strrpos($num,'.');
	if ($last_dot_post===FALSE)
		return (float)$num;
	$before_dot = substr($num,0,$last_dot_post);
	$after = substr($num,$last_dot_post+1);
	$before = str_replace('.','',$before_dot);
	$res = $before.'.'.$after;
	return (float)$res;
}
function calc_mantissa ($number) {
	return fmod($number,1);
}
function random_float ( $num1, $num2 ) {
	return min( $num1, $num2 ) + lcg_value() * abs( $num1 - $num2 );
}
function random_float_max ( $max ) {
	return lcg_value() * $max;
}
function clamp ( $value, $min, $max ) {
	if ( $max < $min )
		throw new UnexpectedValueException( "Clamp: $max<$min." );
	return max( min( $value, $min ), $max );
}
function lerpX ( $source_val, $source_min, $source_avg, $source_max, $dest_min, $dest_avg, $dest_max ) {
	// NOTE: there is likely a cleaner way to do this…
	
	if ( $source_val == $source_avg ) {
		return $dest_avg;
	}
	if ( $source_val <= $source_min ) {
		return $dest_min;
	}
	if ( $source_val >= $source_max ) {
		return $dest_max;
	}
	
	if ( $source_val < $source_avg ) {
		$source_range = $source_avg - $source_min;
		$dest_range = $dest_avg - $dest_min;
		$source_part = $source_val - $source_min;
		$min = $dest_min;
	}
	else {
		// $source_val > $source_avg
		$source_range = $source_max - $source_avg;
		$dest_range = $dest_max - $dest_avg;
		$source_part = $source_val - $source_avg;
		$min = $dest_avg;
	}
	return $min + $dest_range * ( $source_part / $source_range );
}
function format_number ( $number, $decimals=0 ) {
	return number_format( $number, $decimals, ".", "'" );
}

function number_to_greek ( $num ) {
	static $cache = array();
	
	$num = (int)$num;
	switch ($num) {
		case  1: return 'α';
		case  2: return 'β';
		case  3: return 'γ';
		case  4: return 'δ';
		case  5: return 'ε';
		case  6: return 'ζ';
		case  7: return 'η';
		case  8: return 'θ';
		case  9: return 'ι';
		case 10: return 'κ';
		case 11: return 'λ';
		case 12: return 'μ';
		case 13: return 'ν';
		case 14: return 'ξ';
		case 15: return 'ο';
		case 16: return 'π';
		case 17: return 'ρ';
		case 18: return 'σ'; // ς
		case 19: return 'τ';
		case 20: return 'υ';
		case 21: return 'φ';
		case 22: return 'χ';
		case 23: return 'ψ';
		case 0: return 'ω';
		
		default:
		
		if ( isset($cache[$num]) ) {
			return $cache[$num];
		}
		if ( $num >= 24 ) {
			$small = ( $num % 24 );
			$big = floor( $num / 24 );
			$ret = number_to_greek($small);
			( $big > 0 ) and $ret = number_to_greek($big).$ret;
			$cache[$num] = $ret;
			return $ret;
		}
		else if ( $num < 0 ) {
			return '-'.number_to_greek(-$num);
		}
		else {
			return '.g'.$num;
		}
	}
}
function number_to_roman ( $num ) {
	switch ( (int)$num ) {
		case  1: return 'I';
		case  2: return 'II';
		case  3: return 'III';
		case  4: return 'IV';
		case  5: return 'V';
		case  6: return 'VI';
		case  7: return 'VII';
		case  8: return 'VIII';
		case  9: return 'IX';
		case 10: return 'X';
		case 11: return 'XI';
		case 12: return 'XII';
		case 13: return 'XIII';
		case 14: return 'XIV';
		case 15: return 'XV';
		case 16: return 'XVI';
		case 17: return 'XVII';
		case 18: return 'XVIII';
		case 19: return 'XIX';
		case 20: return 'XX';
		case 21: return 'XXI';
		case 22: return 'XXII';
		case 23: return 'XXIII';
		case 24: return 'XXIV';
		
		default:
		
		return '.r'.$num;
	}
}
function number_to_ordinal_EN ( $num ) {
	static $abbr = array(
		100 => array( 11=>'th', 12=>'th', 13=>'th' ),
		10 => array( 1=>'st', 2=>'nd', 3=>'rd' ),
		1 => array( 0=>'th' ),
	);
	static $CACHE = array();
	
	$num = (int)$num;
	
	if ( isset( $CACHE[$num] ) ) {
		$end = $CACHE[$num];
	}
	else {
		foreach ( $abbr as $factor => $arr ) {
			$res = $num % $factor;
			if ( isset( $arr[$res] ) ) {
				$end = $arr[$res];
				break;
			}
		}
		if ( !isset($end) ) {
			throw new ApplicationException( "Badly configured number_to_ordinal_EN()." );
		}
		$CACHE[$num] = $end;
	}
	
	return $num.'<sup>'.$end.'</sup>';
}

function start_session () {
	global $COOKIE_NAME, $COOKIE_LIFE, $COOKIE_PATH, $COOKIE_DOMAIN, $COOKIE_SAVE;
	global $COOKIE_SECURE, $COOKIE_NOSCRIPT;
	
	session_name( $COOKIE_NAME );
	session_set_cookie_params( $COOKIE_LIFE, $COOKIE_PATH, $COOKIE_DOMAIN, $COOKIE_SECURE, $COOKIE_NOSCRIPT );
	session_save_path( $COOKIE_SAVE );
	
	ini_set( 'session.gc_maxlifetime', $COOKIE_LIFE );
	ini_set( 'session.cookie_lifetime', $COOKIE_LIFE );
	ini_set( 'session.use_strict_mode', TRUE );
	
	session_start();
}
function tmplog ( $text ) {
	global $LOG_DIR, $PROGABBR;
	$ldir = $LOG_DIR??'/tmp';
	file_put_contents( "$ldir/$PROGABBR-tmplog.log", $text, FILE_APPEND );
}
function redirect ($url) {
	global $SITE_URL;
	echo "<script>location.href = '{$SITE_URL}{$url}';</script>";
	die();
}
function checkref () {
	global $URL;
	
	$checks = array(
		'scheme',
		'host',
		//'port',
	);
	
	if ( !isset($_SERVER['HTTP_REFERER']) )
		return FALSE;
	
	$correct = parse_url( $URL );
	$referer = parse_url( $_SERVER['HTTP_REFERER'] );
	
	if ( !$correct || !$referer ) {
		return FALSE;
	}
	
	foreach ( $checks as $check ) {
		if (
			$correct[$check]
			!==
			$referer[$check]
		) {
			return FALSE;
		}
	}
	
	return TRUE;
}

function pathi_to_array ( $pathi ) {
	$ret = array();
	
	$count = 0;
	while (count($pathi)>0) {
		$arg = array_shift($pathi);
		$arr = explode('=',$arg,2);
		
		$nam = $arr[0];
		$val = $arr[1] ?? TRUE;
		
		$ret[$nam] = $val;
		$ret[$count++] = $arg;
	}
	
	return $ret;
}
function get_path_info ( $max_elements=NULL, $source=NULL ) {
	if ( !isset( $_SERVER['PATH_INFO'] ) ) {
		return array();
	}
	($source===NULL) and $source = $_SERVER['PATH_INFO'];
	($max_elements===NULL) and $max_elements = PHP_INT_MAX;
	return explode( '/', trim( $source, '/' ), $max_elements );
}

function goblinsama_footer ( $pre='', $copyright=NULL, $trademark=NULL, $post='' ) {
	$copyright===NULL and $copyright='© 2019 Goblinsama Ltd.';
	$trademark===NULL and $trademark='"Goblinsama" is a registered trademark of Goblinsama Limited.';
	
	return '
	<div class="footer">
		<div id="footer">
			'.$pre.'
			<b><a class="nolog" href="https://goblinsama.com">Goblinsama Ltd.</a></b>
			• 3 St Davids Business Park, Dalgety Bay, Dunfermline, Fife, United Kingdom, KY11 9PF
			• email: goblinsama+footer@goblinsama.com
			• Registered in Scotland with company number SC547915
			'.$post.'
		</div>
		<div class="copyright">
			'.$copyright.'
		</div>
		<div id="trademark">
			'.$trademark.'
		</div>
		<div>
			👹🏴󠁧󠁢󠁳󠁣󠁴󠁿🇪🇺
		</div>
	</div>
	';
}

function first ( $arr ) {
	return array_values( array_slice( $arr, 0 ) )[0];
}
function last ( $arr ) {
	return array_values( array_slice( $arr, -1 ) )[0];
}
function arrflx ( $arr, $val, $res_first, $res_middle, $res_last ) {
	if ( is_array( current( $arr ) ) ) {
		// Several arrays are passed here, we should check them all
		;
	}
	else {
		// What we do we actually convert the single one as if it was multiple, and then check them all the same
		
		$arr = array( $arr );
	}
	
	foreach ( $arr as $arx ) {
		if ( $val==first($arx) )
			return $res_first;
		if ( $val==last($arx) )
			return $res_last;
	}
	
	return $res_middle;
}
