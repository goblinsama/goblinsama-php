<?php

/*	Goblinsama-PHP Library
	https://bitbucket.org/goblinsama/goblinsama-php
	
	© 2017-2018 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	© 2015 Lorenzo Petrone "Lohoris" <looris+src@gmail.com> https://lohoris.net
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

class target {
	private static $image_dir='';
	private $name,$url,$image;
	
	public function __construct ($name, $url, $img) {
		$this->name=$name;
		$this->url=$url;
		$this->img=$img;
	}
	public function str ($icon_size=NULL) {
		$SIZ = $icon_size===NULL ? '' : 'width='.$icon_size.' height='.$icon_size;
		$IMG = $this->img ? '<span class="el--icon"><img src="'.static::$image_dir.$this->img.'" '.$SIZ.'></span>' : '';
		return '<a class="el--link" href="'.$this->url.'">'.$IMG.' '.$this->name.'</a>';
	}
	
	public static function print_header ($path='') {
		return '
		<link rel="stylesheet" href="'.$path.'target/target.css" type="text/css" />
		';
	}
	public static function set_image_dir ($value) {
		static::$image_dir=$value;
	}
}
class targets {
	private $items=array();
	
	public function add ($name, $url, $img) {
		array_push($this->items,new target($name,$url,$img));
	}
	public function str ($icon_size=NULL) {
		$ret='<ul class="external-links">';
		foreach ($this->items as $target) {
			$ret.='<li class="el--item">'.$target->str($icon_size).'</li>';
		}
		$ret.='</ul>';
		return $ret;
	}
}

class tagline {
	public $text;
	public $author;
	
	public function __construct ($text, $author=NULL) {
		$this->text=$text;
		$this->author=$author;
	}
}
class taglines {
	private $items=array();
	private $entities;
	
	public function __construct ($entities=true) {
		$this->entities=$entities;
	}
	public function add ($text, $author=NULL) {
		array_push($this->items,new tagline($text,$author));
	}
	public function str () {
		$ret='';
		
		$tagline = $this->items[array_rand($this->items)];
		$text = $this->entities ? htmlspecialchars($tagline->text) : $tagline->text;
		if ($tagline->author!==NULL) {
			$author = $this->entities ? htmlspecialchars($tagline->author) : $tagline->author;
			$ret.='
				<div class="tagline">'.$text.'
					<div class="tl-author">'.$author.'</div>
				</div>
			';
		}
		else {
			$ret.='
				<div class="tagline">'.$text.'</div>
			';
		}
		
		return $ret;
	}
}
