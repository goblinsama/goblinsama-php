<?php

/*	Goblinsama-PHP Library
	https://bitbucket.org/goblinsama/goblinsama-php
	
	© 2017-2019 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	© 2006-2016 Lorenzo Petrone "Lohoris" <looris+src@gmail.com> https://lohoris.net
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

function SendEmailNow ( $to_mail, $to_name, $subject, $bodyhtml_input, $bodyplain=NULL, $use_emogrifier=FALSE ) {
	global $SMTP_HOST, $SMTP_MAIL, $SMTP_USER, $SMTP_PASS, $PROGNAME, $NO_EMAIL_SEND;
	
	if ( !empty($NO_EMAIL_SEND) )
		return NULL;
	
	// Emogrifier
	if ($use_emogrifier) {
		$emogrifier = new \Pelago\Emogrifier($bodyhtml_input,'');
		$bodyhtml = $emogrifier->emogrify();
	}
	else {
		$bodyhtml = $bodyhtml_input;
	}
	
	$mail = new PHPMailer(true);
	{
		PHPMailer::$validator = 'html5';
		
		// Server settings
		//$mail->SMTPDebug = 2;
		$mail->isSMTP();
		$mail->Host = "$SMTP_HOST";
		$mail->SMTPAuth = true;
		$mail->Username = "$SMTP_USER";
		$mail->Password = "$SMTP_PASS";
		$mail->SMTPSecure = "tls";
		$mail->Port = 587;
		
		$mail->CharSet = "UTF-8";
		$mail->ContentType = "text/html; charset=utf-8\r\n";
		
		// Recipients
		$mail->setFrom( "$SMTP_MAIL", "$PROGNAME" );
		$mail->addAddress( $to_mail, $to_name );
		$mail->addReplyTo( "$SMTP_MAIL" );
		
		// Content
		$mail->isHTML( true );
		$mail->Subject = "$subject";
		$mail->Body = $bodyhtml;
		if ( $bodyplain !== NULL )
			$mail->AltBody = $bodyplain;
		
		if ( $mail->send() === FALSE ) {
			return $mail->ErrorInfo;
		}
		return 0;
	}
}
