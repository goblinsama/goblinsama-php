<?php

/*	Goblinsama-PHP Library
	https://bitbucket.org/goblinsama/goblinsama-php
	
	© 2016-2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

// TODO. rewrite this file so that it makes sense

function is_at_least ($user, $level_n) {
	global $user_level_r;
	return ($user['user_level']>=$user_level_r[$level_n]);
}
function am_logged () {
	global $logged_user;
	return $logged_user && is_at_least($logged_user,'user');
}
function am_mod () {
	global $logged_user;
	return (am_logged() && is_mod($logged_user));
}
function is_mod ($user) {
	return is_at_least($user,'mod');
}
function am_admin () {
	global $logged_user,$AM_ADMIN;
	return $AM_ADMIN || (am_logged() && is_admin($logged_user));
}
function is_admin ($user) {
	return is_at_least($user,'admin');
}
function am_daemon () {
	return FALSE;
}
