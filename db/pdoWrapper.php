<?php

/*	Goblinsama-PHP Library
	https://bitbucket.org/goblinsama/goblinsama-php
	
	© 2017,2020 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	© 2006-2015 Lorenzo Petrone "Lohoris" <looris+src@gmail.com> https://lohoris.net
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

class simplePdoWrapper {
	private $pdo;
	private $stm, $lastQuery, $lastParams;
	
	function ConnectMySQL ( $host, $port, $name, $char, $user, $pass ) {
		$this->pdo = new PDO("mysql:host=$host;port=$port;dbname=$name;charset=$char", $user, $pass);
		$this->ConnectCommon();
	}
	function ConnectSQLite ( $filename ) {
		$this->pdo = new PDO("sqlite:$filename");
		$this->ConnectCommon();
	}
	private function ConnectCommon () {
		$this->pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		$this->pdo->setAttribute( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
	}
	
	/// chiamate
	
	// prepara ed esegue la query, e poi esegue un metodo sullo statement
	// i parametri devono essere un array
	public function call ( $method, $query, $params=array() ) {
		$this->lastQuery = $query;
		$this->lastParams = $params;
		
		$this->stm = $this->query($query, $params);
		if ($this->stm===FALSE)
			throw new DatabaseException($this);
		
		return call_user_func( array( $this->stm, $method ) );
	}
	
	// prepara ed esegue la query ($params dev'essere un array)
	public function query ( $query, $params=array() ) {
		$this->lastQuery = $query;
		$this->lastParams = $params;
		
		$this->stm = $this->pdo->prepare($query);
		if ($this->stm->execute($params)===FALSE)
			throw new DatabaseException($this,"Execute error");
		
		return $this->stm;
	}
	
	// esegue un metodo direttamente sul PDO
	public function execute ( $method ) {
		return call_user_func( array( $this->pdo, $method ) );
	}
	
	/// info
	
	public function id () {
		return $this->pdo->lastInsertId();
	}
	public function affected () {
		return $this->stm->rowCount();
	}
	
	/// utility
	
	public function queryId ( $query, $params=array() ) {
		if ($this->query($query,$params)===FALSE)
			throw new DatabaseException($this,"Query error");
		
		return $this->id();
	}
	public function queryAmount ( $query, $params=array() ) {
		if ($this->query($query,$params)===FALSE)
			throw new DatabaseException($this,"Query error");
		
		return $this->affected();
	}
	public function getVar ( $query, $params=array() ) {
		$this->lastQuery = $query;
		$this->lastParams = $params;
		
		$this->stm = $this->query($query,$params);
		if ($this->stm===FALSE)
			throw new DatabaseException($this);
		
		return $this->stm->fetchColumn(0);
	}
	public function queryOne ( $query, $params=array() ) {
		if (($stm=$this->query($query,$params)) === FALSE)
			throw new DatabaseException($this,"Query error");
		
		$affected = $this->affected();
		if ($affected != 1)
			throw new DatabaseException($this,"Affected error [$affected].");
		
		return $stm;
	}
	public function queryMaxOne ( $query, $params=array() ) {
		if (($stm=$this->query($query,$params)) === FALSE)
			throw new DatabaseException($this,"Query error");
		
		$affected = $this->affected();
		if ($affected > 1)
			throw new DatabaseException($this,"Affected error [$affected].");
		
		return $stm;
	}
	public function fetchAllId ( $index_name, $query, $params=array() ) {
		$res = $this->call( 'fetchAll', $query, $params );
		
		$ret = array();
		foreach ( $res as $row ) {
			$ret[$row[$index_name]] = $row;
		}
		return $ret;
	}
	public function fetchAllNV ( $index_name, $value_name, $query, $params=array() ) {
		$res = $this->call( 'fetchAll', $query, $params );
		
		$ret = array();
		foreach ( $res as $row ) {
			$ret[$row[$index_name]] = $row[$value_name];
		}
		return $ret;
	}
	public function fetchList ( $index_name, $query, $params=array() ) {
		$res = $this->call( 'fetchAll', $query, $params );
		
		$ret = array();
		foreach ( $res as $row ) {
			array_push( $ret, $row[$index_name] );
		}
		return $ret;
	}
	public function deleteList ( $table_name, $index_name, $list ) {
		$count_list = count( $list );
		
		$WH = "0".str_repeat( " OR $index_name=?", $count_list );
		$cound_effect = $this->queryAmount( "DELETE FROM $table_name WHERE $WH", $list );
		
		$cound_effect==$count_list or except( 'UnexpectedValueException', "Delete an incorrect amount of rows [$cound_effect != $count_list]" );
		return $cound_effect;
	}
	
	/// shortcuts
	
	public function fetchRow ( $query, $params=array() ) {
		return $this->call( 'fetch', $query, $params );
	}
	public function fetchAll ( $query, $params=array() ) {
		return $this->call( 'fetchAll', $query, $params );
	}
	
	/// transactions
	
	public function begin () {
		return $this->query('BEGIN');
	}
	public function commit () {
		return $this->query('COMMIT');
	}
	public function rollback () {
		return $this->query('ROLLBACK');
	}
	
	/// errori
	
	public function getError () {
		global $DEBUG_EX;
		
		$text = print_rr($this->stm,$this->stm->errorInfo(),$this->pdo->errorInfo(),$this->lastQuery,$this->lastParams);
		tmplog($text);
		
		if ( $DEBUG_EX || am_admin() )
			return $text;
		return '';
	}
	public function getLastQuery () {
		global $DEBUG_EX;
		
		return ($DEBUG_EX || am_admin() || am_daemon()) ? array( $this->lastQuery, $this->lastParams ) : '–';
	}
	
	///
}
