<?php

/*	Goblinsama-PHP Library
	https://bitbucket.org/goblinsama/goblinsama-php
	
	© 2017-2018 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

class TimeLap {
	private $name_format = "[%?.?s]";
	private $time_format = "%10.4f at %15.3f";
	private $mem_format = "[%11d] tot [%11d]";
	
	private $title;
	private $laps, $prev;
	private $opt;
	private $_print_now_buffer = "";
	
	public function __construct ( $title, $opt=array() ) {
		$name_length = $opt['name_length'] ?? 20;
		
		$this->name_format = str_replace( "?", "$name_length", $this->name_format );
		$this->title = $title;
		$this->opt = array_flip( $opt );
		
		$this->laps = array();
		$this->prev = NULL;
		
		$pb = $this->Lap( "$title" );
		if ( isset( $this->opt['printnow'] ) ) {
			$this->_print_now_buffer = "\n\n$pb";
		}
	}
	
	public function Lap ( $name=NULL ) {
		if ( $name === NULL ) {
			$name = "Lap #".count( $this->laps );
		}
		$time = microtime(TRUE);
		
		$lap = array(
			'name' => $name,
			'time' => $time,
		);
		isset( $this->opt['mem'] ) and $lap['mem'] = memory_get_usage();
		
		if ( !isset( $this->opt['single'] ) ) {
			array_push( $this->laps, $lap );
		}
		
		$ret = NULL;
		if ( isset( $this->opt['printnow'] ) ) {
			$ret = $this->_print_now_buffer;
			$this->_print_now_buffer = "";
			
			if ( $this->prev!==NULL ) {
				// Prints the second part of the previous row
				
				$ret .= $this->PrintDiff( $this->prev, $lap, FALSE )."\n";
			}
			
			// Prints the first part of the current row
			
			$ret .= sprintf( $this->name_format." ", $lap['name'] );
		}
		else if ( isset( $this->opt['print'] ) ) {
			$ret = $this->PrintLast( $lap );
		}
		
		$this->prev = $lap;
		
		return $ret;
	}
	public function PrintLast ( $lap ) {
		return $this->PrintDiff( $this->prev, $lap );
	}
	public function PrintAll () {
		isset( $this->opt['single'] ) and except( 'BadMethodCallException', "Cannot call PrintAll if opt:single is ON." );
		
		$ret = '';
		$ret .= "\n";
		$former_lap = NULL;
		foreach ( $this->laps as $lap ) {
			if ( $former_lap === NULL ) {
				// prima entry, ovvero lo start
				$ret .= "Starting at {$lap['time']}: {$lap['name']}\n";
			}
			else {
				$ret .= $this->PrintDiff( $former_lap, $lap )."\n";
			}
			
			$former_lap = $lap;
		}
		
		// l'ultimo
		$ret .= $this->PrintDiff( $former_lap, $lap )."\n";
		
		// totale
		$time = microtime(TRUE);
		$total_diff = $time - $this->laps[0]['time'];
		$ret .= sprintf( $this->name_format." ".$this->time_format, "TOTAL", $total_diff, $time )."\n";
		
		return $ret;
	}
	public function __toString () {
		return $this->PrintAll();
	}
	
	private function PrintDiff ( $prev, $last, $NAME=TRUE ) {
		$ret = "";
		
		if ( $NAME ) {
			$ret .= sprintf( $this->name_format." ", $prev['name'] );
		}
		
		$diff_t = $last['time'] - $prev['time'];
		$ret .= sprintf( $this->time_format, $diff_t, $prev['time'] );
		
		if ( isset( $this->opt['mem'] ) ) {
			$diff_m = $last['mem'] - $prev['mem'];
			$ret .= "; mem: ".sprintf( $this->mem_format, $diff_m, $prev['mem'] );
		}
		
		return $ret;
	}
}
