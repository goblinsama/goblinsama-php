<?php

/*	Goblinsama-PHP Library
	https://bitbucket.org/goblinsama/goblinsama-php
	
	© 2017-2019 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	© 2006-2016 Lorenzo Petrone "Lohoris" <looris+src@gmail.com> https://lohoris.net
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

class PageException extends Exception {
	public function __construct ( $message="" ) {
		$this->message = $message;
		parent::__construct( $message );
	}
	public function __toString () {
		return "Page error: {$this->message}";
	}
}
class PageWarning extends PageException {
}

class Page {
	private $page;
	private $type;
	private $end;
	private $acontrol;
	private $response;
	private $aextra;
	
	private $out; // usata da "json"
	private $status;
	private $status_message;
	private $status_message_div;
	private $status_class;
	private $count_success;
	private $brief=FALSE;
	private $ended=FALSE;
	private $err_extra='';
	
	private $DEBUG=FALSE;
	private $DEBUG_PLAIN=FALSE;
	
	public function __construct ( $type="plain", $head="", $end="" ) {
		$this->type = $type;
		
		if ( $this->type=="html" ) {
			$ctt = 'Content-Type:text/html; charset=utf-8';
		}
		else if ( $this->type=="array" ) {
			$ctt = 'Content-Type:text/plain; charset=utf-8';
		}
		else if ( $this->type=="bin" ) {
			$ctt = 'Content-Type:binary/octet-stream;';
		}
		else if ( $this->type=="hex" ) {
			$ctt = 'Content-Type:text/plain; charset=ISO-8859-15';
		}
		else if ( $this->type=="json" ) {
			ob_start();
			$ctt = 'Content-Type:application/json; charset=utf-8';
		}
		else if ( $this->type=="rss" ) {
			$ctt = 'Content-Type:application/rss+xml; charset=utf-8';
		}
		else {
			$ctt = 'Content-Type:text/plain; charset=utf-8';
		}
		if ( !headers_sent() ) {
			header( $ctt );
		}
		
		if ( $this->type == "array" ) {
			$this->page = array();
			$this->end = array();
			
			array_push( $this->page, $head );
			array_push( $this->end, $end );
		}
		else {
			$this->page = $head;
			$this->end = $end;
		}
		$this->acontrol = array();
		$this->aextra = array();
		$this->response = array();
		
		$this->out = '';
		$this->status = '';
		$this->status_message = NULL;
		$this->status_message_div = NULL;
		$this->status_class = '';
		$this->alert_class = '';
		$this->count_success = 0;
		
		register_shutdown_function( array( $this, 'ShutdownHandler' ) );
	}
	public function set_debug ($DEBUG=TRUE, $PLAIN=FALSE) {
		$this->DEBUG=$DEBUG;
		$this->DEBUG_PLAIN=$PLAIN;
	}
	public function set_brief ($val=TRUE) {
		$this->brief = $val;
	}
	public function flush () {
		if ( $this->type == "array" ) {
			$this->page = array();
		}
		else {
			$this->page = '';
		}
	}
	public function sync () {
		if ( $this->type == "hex" ) {
			echo bin2hex( $this->page );
		}
		else if ( $this->type == "json" ) {
			// json non supporta multipli sync, dunque dobbiamo fare in modo di fare comunque output tutto alla fine
			$this->out .= $this->page;
		}
		else if ( $this->type == "array" ) {
			print_r( $this->page );
		}
		else {
			echo $this->page;
		}
		if ( $this->DEBUG ) {
			if ( $this->DEBUG_PLAIN ) {
				$log=array("page"=>$this->page);
			}
			else {
				$log=array();
			}
			$log["hex"]=bin2hex($this->page);
			tlog($log);
		}
		$this->flush();
	}
	public function add ( $text ) {
		// todo. invece di renderarla subito, salvare in un array e renderare una volta che fa sync()
		if ( ($this->type=="bin" || ($this->type=="hex") ) && is_int($text) ) {
			$this->page .= chr( $text );
		}
		else if ( $this->type == "array" ) {
			array_push( $this->page, $text );
		}
		else {
			$this->page .= $text;
		}
	}
	public function command ($cmd, $seq=0) {
		$this->add(packb($cmd,1));
		if ($seq>0)
			$this->add(packb($seq,4));
		$this->sync();
	}
	public function ok ($message=NULL, $class=NULL) {
		$this->status_message = $message;
		$this->status_class = $class===NULL ? 'alert-success' : $class;
		
		if ($this->type=="json") {
			($this->status!="ERR") and $this->status="OK";
		}
		else if ($this->type=="html") {
			if (!empty($message)) {
				$this->add('<div class="alert alert-success">'.$message.'</div>');
			}
		}
		else {
			$this->add(1);
		}
	}
	public function no ( $err=NULL, $WARNING=FALSE ) {
		if ( $this->status_class == 'alert-danger' ) {
			$this->status_message .= "<p>$err</p>";
		}
		else {
			$this->status_class = 'alert-danger';
			$WARNING and $this->alert_class = 'warning';
			$this->status_message = "$err";
		}
		
		if ( $this->type == 'html' ) {
			if ( !empty($err) ) {
				$this->add( '<div class="alert '.$this->status_class.'">'.$err.'</div>'.$this->err_extra );
			}
		}
		else {
			if ( $this->type == "json" ) {
				( $this->status != "ERR" ) and $this->status = "NO";
				return;
			}
			else {
				$this->add( -1 );
			}
			if ( $err !== NULL ) {
				$this->add( "$err" );
			}
		}
	}
	public function err_extra ( $content ) {
		$this->err_extra = $content;
	}
	public function error ($message, $NL2BR=TRUE) {
		// NOTA: non logga questi errori, essendo piuttosto comuni tra web crawler e roba del genere
		
		$this->status_message=$message;
		$this->status_class="alert-danger";
		
		$this->status="ERR";
		throw new PageException($message);
		/*
		global $gdata; // todo. è orrendo metterla global
		
		$this->flush();
		if ($this->type=="bin" || $this->type=="hex") {
			$this->add(0); // ERR
		}
		
		if ($NL2BR) {
			$this->add(nl2br($message));
		}
		else {
			$this->add($message);
		}
		$this->nl();
		$this->add("data length: ".strlen($gdata));
		$this->nl();
		$this->add(bin2hex($gdata));
		//$this->add(print_r(debug_backtrace(),true));
		die();
		*/
	}
	public function nl () {
		if ($this->type=="html") {
			$this->add("<br>");
		}
		else {
			$this->add("\n");
		}
	}
	public function status_message_div_set ($name) {
		$this->status_message_div=$name;
	}
	public function control ($name, $status, $message="") {
		array_push($this->acontrol,array(
			"name" => $name,
			"status" => $status,
			"message" => $message,
		));
		if ($status=="success")
			$this->count_success++;
		return "$name:$status:$message";
	}
	public function control_error_add ($name, $message="") {
		$this->control($name,"error",$message);
	}
	public function control_error_die ($name, $message="") {
		$this->control_error_add($name,$message);
		throw new PageException();
	}
	public function control_error () {
		foreach ($this->acontrol as $el) {
			if ($el["status"]=="error") return true;
		}
		return false;
	}
	public function extra ($arr) {
		array_push($this->aextra,$arr);
	}
	public function response ($element, $value) {
		$this->response[$element]=$value;
	}
	public function successes () {
		return $this->count_success;
	}
	public function end () {
		if ( $this->ended )
			return;
		$this->ended = TRUE;
		
		$this->add( $this->end );
		$this->sync();
		if ( $this->type == "json" ) {
			$ob = ob_get_contents();
			if ( $ob ) {
				ob_end_clean();
			}
			
			$this->response[ "status" ] = $this->status;
			$this->response[ "status_message" ] = $this->status_message;
			
			if ( !$this->brief || $this->status == "NO") {
				$this->response[ "control" ] = $this->acontrol;
			}
			if ( !$this->brief ) {
				$this->response[ "status_message_div" ] = $this->status_message_div;
				$this->response[ "status_class" ] = $this->status_class;
				$this->response[ "alert_class" ] = $this->alert_class;
				$this->response[ "extra" ] = $this->aextra;
				$this->response[ "page" ] = $this->out;
			}
			echo json_encode( $this->response );
		}
	}
	public function __destruct () {
		if ( $this->type == "json" || $this->type == "array" ) {
			$this->end();
		}
	}
	
	public function ShutdownHandler () {
		// TODO. now it DOES show all the notices and stuff, but still doesn't halt the program in that case
		
		$err = error_get_last();
		if ( !$err )
			return;
		
		// if ( $err['type'] === E_ERROR )
		
		$this->no( print_rh( $err ) ); // NOTE: should have printed the backtrace, but this can't be done
		
		$this->end();
		die();
	}
}

function DeBuffer ( $string='' ) {
	global $DEBUG_Buffer;
	
	isset($DEBUG_Buffer) or $DEBUG_Buffer = '';
	
	$DEBUG_Buffer .= $string;
	
	return $DEBUG_Buffer;
}
function DeBufferClear () {
	global $DEBUG_Buffer;
	$ret = $DEBUG_Buffer;
	$DEBUG_Buffer = '';
	return $ret;
}
function Page_DEBUG ( $string ) {
	if ( isset($GLOBALS['page']) ) {
		$GLOBALS['page']->add( $string );
	}
	else {
		echo $string;
	}
}
