/*	Goblinsama-PHP Library
	https://bitbucket.org/goblinsama/goblinsama-php
	
	© 2017-2018 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	© 2012-2014 Lorenzo Petrone "Lohoris" <looris+src@gmail.com> https://lohoris.net
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

function page_loaded () {
	$('.tooltip').remove();
	$('[title]').tooltip({html: true});
	
	if ( typeof extra_page_loaded === 'function' ) {
		extra_page_loaded();
	}
}
function tab_show ( name ) {
	$('a[href="#'+name+'"]').click();
}
function content_visit ( target, tab, extra ) {
	content_post( target+'/Content', { 'tab':tab, 'extra':extra } );
	return false;
}
function scroll_to ( el, up ) {
	$( window ).scrollTop( $(el).offset().top - up );
}

function load_start ( elem ) {
	$(elem).hide();
	$(elem).after(loading);
}
function load_end ( elem ) {
	var img = $( elem ).next( 'img.loading' );
	if ( img ) img.remove();
	$( elem ).show();
}

var last_div; // da usare solo per inserire gli errori ingestibili in altro modo
function advanced_post_target ( target, type, div, vars ) {
	return advanced_post(target,div,undefined,vars,undefined,undefined,type);
}
function command_post ( target, vars, handler ) {
	return advanced_post( target, '#command', undefined, vars, handler );
}
function content_post ( target, vars, handler ) {
	return advanced_post( target, '#content', undefined, vars, handler );
}
function advanced_post ( target, div, load, vars, handler, baseURL, div_type ) {
	vars = typeof vars !== 'undefined' ? vars : {};
	handler = typeof handler !== 'undefined' ? handler : advanced_handler;
	baseURL = typeof baseURL !== 'undefined' ? baseURL : APIURL;
	div_type = typeof div_type !== 'undefined' ? div_type : 'replace';
	
	var data="";
	last_div = div;
	
	vars['back']={'div':div, 'div_type':div_type, 'load':load};
	//console.debug(vars);
	
	if (typeof load=='string') {
		vars['back']['load_end']=load;
		load_start(load);
	}
	else if (typeof load=='boolean') {
		jQuery(div).html(loading);
	}
	
	return jQuery.post(baseURL+target,vars,handler).fail(handler);
}
function advanced_handler ( data_x ) {
	try {
		var data = data_x['responseJSON'] ? data_x['responseJSON'] : data_x;
		
		var alert_class;
		
		if ( data['back'] && data['back']['load_end'] ) {
			load_end( data['back']['load_end'] );
		}
		
		if ( data['status'] == 'OK' ) {
			alert_class = data['alert_class'] ? data['alert_class'] : 'success';
			
			var page = data['page'];
			var div_tgt = data['back']['div'];
			if ( typeof( page )!='undefined' && div_tgt.length>0 ) {
				var tgt = jQuery( div_tgt );
				var div_type = data['back']['div_type'];
				
				if ( div_type == 'replace' ) {
					tgt.html( page );
				}
				else if ( div_type == 'first' ) {
					tgt.prepend( page );
				}
				else if ( div_type == 'last' ) {
					tgt.append( page );
				}
				else {
					throw 'Unknown div_type ['+div_type+']';
				}
				
				tgt.parent().find( '.loading' ).hide();
				tgt.show();
			}
		}
		else if ( data['status'] == 'NO' ) {
			alert_class = data['alert_class'] ? data['alert_class'] : 'danger';
		}
		else {
			// todo. trappare in qualche modo gli errori non formattati
		}
		
		if ( data['control'] ) data['control'].forEach( function( val ) {
			do_control( val['name'], val['status'], val['message'] );
		} );
		
		if ( data['status_message'] ) {
			var status_msg = status_message( alert_class, data["status_message"] );
			if (data['status_message_div']) {
				jQuery( data['status_message_div'] ).html( status_msg );
				scroll_to( data['status_message_div'], 64 );
			}
			else {
				jQuery( data['back']['div'] ).prepend( status_msg );
				scroll_to( data['back']['div'], 64 );
			}
		}
		else if ( data['status_message_div'] ) {
			// clears it
			jQuery( data['status_message_div'] ).html( "" );
		}
		
		page_loaded();
	}
	catch ( er ) {
		console.log( 'Error '+er );
		console.debug( er.stack );
		console.debug( data );
		console.debug( jQuery( last_div ) );
		
		jQuery( last_div ).before( '<div class="alert alert-danger"><p>'+nl2br( escapeHtml( data.responseText ) )+'</p><p>'+nl2br( er )+'</p></div>' );
		
		return false;
	}
	
	return (data['status']=='OK');
}

function status_message ( status, message ) {
	return '\
		<div class="alert alert-'+status+' alert-block">\
			<span class="close" data-dismiss="alert">&times;</span>\
			'+message+'\
		</div>\
	';
}

function reload ( target ) {
	return advanced_post(target+"?reload", "#"+target, null, {});
}

//// rimuovere, in favore delle nuove
function help_set ( div, help, status, message ) {
	var set_message=(typeof message!='undefined');
	
	var cla = div.hasClass('inline') ? "control-group inline" : "control-group";
	div.attr("class",cla+" "+status);
	if (set_message) {
		help.html(message);
	}
}
function err2warn ( elem ) {
	var target=$(elem).parent().parent();
	if ($(target).hasClass('error')) {
		$(target).addClass('warning');
		$(target).removeClass('error');
	}
}
function any2warn ( elem ) {
	var target=$(elem).parent().parent();
	$(target).addClass('warning');
	$(target).removeClass('error');
	$(target).removeClass('success');
	$(target).removeClass('info');
}
function cancel_status ( elem ) {
	var target=$(elem).parent().parent();
	$(target).removeClass('warning');
	$(target).removeClass('error');
	$(target).removeClass('success');
	$(target).removeClass('info');
}
function change_status ( elem, status, message ) {
	var target,help_id;
	if (typeof(elem)=='string') {
		target = $('#'+elem+'_div');
		help_id = elem+'_help';
	}
	else {
		target = $(elem).parent().parent();
		help_id = $(elem).attr('id')+'_help';
	}
	help_set(target,$('#'+help_id),status,message);
}

//// nuove funzioni
function x2warn ( elem ) {
	if (elem.hasClass('error')) {
		changeStatus(elem,'warning');
	}
	else if (elem.hasClass('success')) {
		changeStatus(elem,'info');
	}
}
function cancelStatus ( elem ) {
	elem.removeClass('warning');
	elem.removeClass('error');
	elem.removeClass('success');
	elem.removeClass('info');
}
function changeStatus ( elem, status, message ) {
	cancelStatus(elem);
	elem.addClass(status);
	elem.find('.help-inline').html(message);
}
function controlStatus ( elem, status, message ) {
	var cnt=elem.closest('.control-group');
	changeStatus(cnt,status,message);
}
