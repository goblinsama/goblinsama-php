/*

*/

// http://stackoverflow.com/a/12034334/207655
// https://github.com/janl/mustache.js/blob/master/mustache.js#L82
var entityMap = {
	"&": "&amp;",
	"<": "&lt;",
	">": "&gt;",
	'"': '&quot;',
	"'": '&#39;',
	"/": '&#x2F;'
};
function escapeHtml (string) {
	return String(string).replace(/[&<>"'\/]/g, function (s) {
		return entityMap[s];
	});
}

// http://stackoverflow.com/a/7467863/207655
// http://phpjs.org/functions/nl2br:480
function nl2br (str, is_xhtml) {
	var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
	return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

// https://stackoverflow.com/a/1026087
function ucFirst ( str ) {
	return str.charAt(0).toUpperCase() + str.slice(1);
}

// https://magp.ie/2011/01/20/addslashes-and-stripslashes-in-javascript/
function addslashes ( str ) {
	str = str.replace( /\\/g, '\\\\' );
	str = str.replace( /\'/g, '\\\' ');
	str = str.replace( /\"/g, '\\"' );
	str = str.replace( /\0/g, '\\0' );
	return str;
}
function stripslashes ( str ) {
	str = str.replace( /\\'/g,  '\'' );
	str = str.replace( /\\"/g,  '"' );
	str = str.replace( /\\0/g,  '\0' );
	str = str.replace( /\\\\/g, '\\' );
	return str;
}
