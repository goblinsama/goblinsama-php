<?php

/*	Goblinsama-PHP Library
	https://bitbucket.org/goblinsama/goblinsama-php
	
	© 2017-2020 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	© 2006-2016 Lorenzo Petrone "Lohoris" <looris+src@gmail.com> https://lohoris.net
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

function ParseMD ( $string ) {
	static $md=NULL;
	
	if ( $md === NULL ) {
		$md = new Parsedown();
		$md->setBreaksEnabled( TRUE );
	}
	
	return $md->Text( $string );
}

class Doc {
	protected $id, $filename, $title;
	protected $raw_text, $show_text;
	
	public function __construct ( $id, $filename, $title ) {
		$this->id = $id;
		$this->filename = $filename;
		$this->title = $title;
	}
	
	protected function Load () {
		global $DOC_DIR;
		
		if ( $this->raw_text )
			return TRUE;
		
		$this->raw_text = file_get_contents( "$DOC_DIR/$this->filename.md" ); // TODO. don't hardcode the extension
		$this->raw_text===FALSE and except( 'NotFoundException', "Can't find data" );
		strlen($this->raw_text)>=128 or except( 'NotFoundException', "Invalid data" );
		
		$this->show_text = nl2br( $this->raw_text );
		
		return FALSE;
	}
	
	public function Show ( $BLANK_LINKS=FALSE ) {
		$this->Load();
		
		$body = ParseMD($this->raw_text);
		$BLANK_LINKS and $body=blank_links($body);
		
		return '
		<div class="doc_body">
			'.$body.'
		</div>
		';
	}
}

class Docs {
	protected $docs = array();
	protected $fname = array();
	
	public function Add ( $id, $filename, $title ) {
		$doc = new Doc( $id, $filename, $title );
		
		$this->docs[$id] = $doc;
		$this->fname[$filename] = $doc;
		
		return $doc;
	}
	
	public function Get ( $id ) {
		isset($this->docs[$id]) or except( 'NotFoundException', "Incorrect label [$id]." );
		return $this->docs[$id];
	}
	public function GetFromFilename ( $fname ) {
		isset($this->fname[$fname]) or except( 'NotFoundException', "Incorrect filename [$fname]." );
		return $this->fname[$fname];
	}
	public function GetList () {
		return array_keys( $this->docs );
	}
}
